from django.conf.urls import include, url
from pfam_maps import views
#from pfam_maps.forms import ExRegistrationForm
#from registration.backends.default.views import RegistrationView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^logs/portal/$', views.logs_portal, name='logs_portal'),
    url(r'^logs/query/$', views.query_logs, name='query_logs'),
    url(r'^logs/download/$', views.download_logs, name='download_logs'),
    url(r'^logs/$', views.logs, name='logs'),
    url(r'^evidence/$', views.evidence_portal, name='evidence_portal'),
    url(r'^evidence/download/$', views.download_pfam, name='download_pfam'),
    url(r'^evidence/(?P<pfam_name>[\w-]+)/$', views.evidence, name='evidence'),
    url(r'^conflicts/$', views.conflict_portal, name='conflict_portal'),
    url(r'^resolved/$', views.resolved_portal, name='resolved_portal'),
    url(r'^conflicts/(?P<conflict_id>.+)/$', views.conflicts, name = 'conflicts'),
    url(r'^resolved/(?P<conflict_id>.+)/$', views.resolved, name = 'resolved'),
    url(r'^vote/conflicts/(?P<assay_id>CHEMBL\d+)/(?P<conflict_id>.+)/$', views.vote_on_assay , name =  'vote_on_assay'),
    url(r'^revoke/resolved/(?P<assay_id>CHEMBL\d+)/(?P<conflict_id>.+)/$', views.revoke_assay , name  =  'revoke_assay'),
    url(r'^accounts/login/$', views.login_view, name= 'login'),
    url(r'^accounts/logout/$', views.logout_view, name= 'logout'),
    url(r'^accounts/profile/$', views.user_portal, name= 'user_portal'),
    url(r'^about/$', views.about, name= 'about'),
    url(r'^accounts/register/$', views.registration_view, name = 'user_registration'),
    ]


